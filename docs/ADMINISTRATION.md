# Administration instructions for super-users

This document describes how to perform some common tasks for AutoFeedback superusers, such as managing modules.
It does not cover how to perform tutor-level / teaching assistant-level tasks.

[[_TOC_]]

## Activating admin mode

Many actions with potentially large impact are only available from "admin mode", even if you are a superuser.
This is equivalent to `sudo`-level access in Linux: it enables potentially destructive actions such as deleting entire modules, and for that reason it is disabled by default.

Click on your username on the top-right, and select "Enable Admin Mode".
This will return you to the landing page, this time in "admin mode":

![Screenshot showing admin mode enabled](img/admin-mode-enabled.png)

You will notice that the top bar is now in bright yellow (indicating admin mode is on), and that a new admin-only section called "Jobs" is visible.
New admin-only actions may become available in other pages as well.

Once you are done with those admin-level actions, we *heavily* recommend that you disable admin mode by clicking on your username at the top, then selecting "Disable Admin Mode".

## Managing modules

When you first log into AutoFeedback, going to the "Modules" section will show this:

![Screenshot showing no modules are listed and no create button outside of admin mode](img/no-modules.png)

In order to manage modules, you must activate the "admin mode" as shown [above](#activating-admin-mode).
Once you do that, the "Modules" section will now show a "Create Module" button:

![Screenshot showing the "Create Module" button in admin mode](img/admin-no-modules.png)

You can now click go into "Create Module", fill in the form and press "Submit".

Other actions on the module can be performed from the module's main page.
Tutors can "Edit" the module to rename it, and superusers can "Delete" the module while in admin mode.
