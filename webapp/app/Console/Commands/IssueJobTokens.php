<?php

/**
 *  Copyright 2021 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Console\Commands;

use App\Http\Controllers\API\JobController;
use App\User;
use App\ZipSubmission;
use Database\Seeders\AssessorUserSeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

/**
 * This command issues the internal tokens needed to download a submission
 * and upload the results. This command is mostly for internal testing and
 * development of the Docker images used to run jobs.
 */
class IssueJobTokens extends Command
{
    const COMMAND = 'autofeedback:issue-job-tokens';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = self::COMMAND . ' {jobID : ID of the job}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Issues tokens for downloading the build ZIP and uploading the results of a job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $jobId = $this->argument('jobID');
        $job = ZipSubmission::find($jobId);
        if (!$job) {
            $this->error("No ZipSubmission exists with ID $jobId");
            return 1;
        }

        /** @var User $assesorUser */
        $assesorUser = User::where('email', AssessorUserSeeder::ASSESSOR_USER_EMAIL)->first();
        if (!$assesorUser) {
            $this->error('Assessor user does not exist: please run the AssessorUserSeeder');
            return 2;
        }

        $randomSuffix = Str::random(16);
        $downloadToken = $assesorUser->createToken("download_build_zip--$jobId-$randomSuffix", [
            JobController::DOWNLOAD_COMBINED_CAP_PREFIX . $jobId
        ]);
        $resultsToken = $assesorUser->createToken("store_results-$jobId-$randomSuffix", [
            JobController::STORE_RESULTS_CAP_PREFIX . $jobId
        ]);

        $this->line("Single-use tokens for job $jobId:");
        $this->line("  * Build ZIP download token: " . $downloadToken->plainTextToken);
        $this->line("  * Results upload token: " . $resultsToken->plainTextToken);

        return 0;
    }
}
