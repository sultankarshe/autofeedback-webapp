<?php

/**
*  Copyright 2021 Aston University
*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
*
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Http\Controllers\API;


use App\Events\MavenBuildJobStatusUpdated;
use App\Http\Controllers\Controller;
use App\Jobs\MarkSubmissionJob;
use App\Zip\ExtendedZipArchive;
use App\ZipSubmission;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class JobController extends Controller
{
    use GeneratesJSON;

    const MAX_BUILD_RESULTS_SIZE_KB = 10000;

    const DOWNLOAD_COMBINED_CAP_PREFIX = 'jobs:download_build_zip:';
    const STORE_RESULTS_CAP_PREFIX = 'jobs:store_results:';

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth:sanctum', 'job.api.range']);
    }

    /**
     * Retrieves a ZIP with the combination of the student's submission and the file
     * overrides, ready to be built to produce the automated feedback. This can use
     * either a general personal access token (e.g. from a tutor in the module), or
     * a limited single-use token (e.g. from a Kube job).
     */
    public function downloadBuildZip(Request $request, ZipSubmission $job) {
        if (!$request->user()->tokenCan(self::DOWNLOAD_COMBINED_CAP_PREFIX . $job->id)) {
            return $this->jsonForbidden('Invalid token.',
            'Token does not have the capability to download a combined ZIP for this submission.'
            );
        }
        $this->revokeSingleUseToken($request);

        $canViewJob = $request->user()->can('view', $job);
        $canViewModelSolution = !$job->assessment
            || $request->user()->can('viewModelSolution', $job->assessment->assessment);
        if (!$canViewJob || !$canViewModelSolution) {
            return $this->jsonForbidden('Insufficient permissions.',
                'You do not have the necessary permissions to download the build ZIP.');
        }

        try {
            // GENERATE ZIP
            $buildWD = $job->setUpWorkingDirectory();
            $tempPath = tempnam(sys_get_temp_dir(), 'combined');
            ExtendedZipArchive::zipTree($buildWD, $tempPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        } catch (\Exception $e) {
            if (isset($tempPath) && file_exists($tempPath)) {
                unlink($tempPath);
            }

            Logger::error($e);
            return $this->jsonServerError(
                'Error while generating ZIP file.',
                'There was a problem generating the combined ZIP file - please contact the system administrator.');
        }

        return response()->download($tempPath, 'build-sub_' . $job->id . '.zip')->deleteFileAfterSend(true);
    }

    /**
     * Replaces the results of this job from those of the provided ZIP, giving the job the provided status code.
     *
     * @see ZipSubmission::collectResultsFromZIP() for details on the expected format of the ZIP file.
     */
    public function storeResults(Request $request, ZipSubmission $job) {
        if (!$request->user()->tokenCan(self::STORE_RESULTS_CAP_PREFIX . $job->id)) {
            return $this->jsonForbidden('Invalid token.',
                'Token does not have the capability to upload build results for this submission.'
            );
        }
        $this->revokeSingleUseToken($request);

        $canViewJob = $request->user()->can('view', $job);
        $canUploadResults = $request->user()->can('storeResults', $job);
        if (!$canViewJob || !$canUploadResults) {
            return $this->jsonForbidden('Insufficient permissions.',
                'You do not have the necessary permissions to upload build results.');
        }

        $validator = Validator::make($request->all(), [
            'resultsfile' => ['required', 'file', 'mimes:zip', 'max:' . self::MAX_BUILD_RESULTS_SIZE_KB],
            'statuscode' => ['required', 'numeric', 'integer', 'min:0' ],
        ]);
        if ($validator->fails()) {
            return $this->jsonInvalidRequest($validator);
        }

        $resultsFile = $request->file('resultsfile');
        $statusCode = (int) $request->get('statuscode');
        DB::transaction(function() use ($job, $resultsFile, $statusCode) {
            $job->status = $statusCode;
            $job->save();

            event(new MavenBuildJobStatusUpdated($job));
            Log::info("Maven build for '"
                . $job->diskPath
                . "' completed with return value $statusCode");

            $job->resultFiles()->delete();
            $job->collectResultsFromZIP($resultsFile);
        });

        if ($job->assessment) {
            MarkSubmissionJob::dispatch($job->assessment->id);
        }
        return $this->jsonResponse('OK');
    }

    /**
     * @param Request $request
     */
    private function revokeSingleUseToken(Request $request): void
    {
        if (!$request->user()->tokenCan('*')) {
            // This is a single-capability, one-time use token: revoke it
            $request->user()->currentAccessToken()->delete();
        }
    }
}
