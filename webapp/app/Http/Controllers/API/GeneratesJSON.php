<?php

/**
 *  Copyright 2021 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Http\Controllers\API;

use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Validator;


trait GeneratesJSON
{

    protected function jsonForbidden($title, $description)
    {
        return $this->jsonError($title, $description, 403);
    }

    private function jsonError($title, $description, $code = 400): JsonResponse
    {
        return $this->jsonResponse($title, [
            'errors' => ['internal' => __($description)],
        ], $code);
    }

    private function jsonResponse($message, $data = [], $code = 200)
    {
        return response()->json(array_merge(
            ['message' => __($message)],
            $data
        ), $code);
    }

    protected function jsonBadRequest($title, $description)
    {
        return $this->jsonError($title, $description, 400);
    }

    protected function jsonInvalidRequest(Validator $validator)
    {
        return $this->jsonResponse(
            'Invalid request.',
            [
                'errors' => $validator->errors()->toArray()
            ], 400);
    }

    protected function jsonServerError($title, $description)
    {
        return $this->jsonError($title, $description, 500);
    }
}
