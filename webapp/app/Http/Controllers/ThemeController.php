<?php

/**
 *  Copyright 2020-2022 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;

class ThemeController extends Controller
{
    public const LIGHT_THEME = 'light';
    public const DARK_THEME = 'dark';
    private const SESSION_KEY = 'theme';

    public function toggle(): RedirectResponse
    {
        session([self::SESSION_KEY => self::isLightTheme() ?  self::DARK_THEME : self::LIGHT_THEME]);
        return redirect()->back();
    }

    public static function isLightTheme(): bool {
        return session(self::SESSION_KEY) !== self::DARK_THEME;
    }

}
