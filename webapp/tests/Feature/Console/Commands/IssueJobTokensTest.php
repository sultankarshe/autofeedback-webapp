<?php

/**
 *  Copyright 2021 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Tests\Feature\Console\Commands;

use App\Console\Commands\IssueJobTokens;
use App\User;
use App\ZipSubmission;
use Database\Seeders\AssessorUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class IssueJobTokensTest extends TestCase
{
    use RefreshDatabase;

    public function testMissingID()
    {
        $this->artisan(IssueJobTokens::COMMAND . ' 42')
            ->expectsOutput('No ZipSubmission exists with ID 42')
            ->assertExitCode(1);
    }

    public function testNoAssessor() {
        Storage::fake();
        $this->seed();
        User::where('email', AssessorUserSeeder::ASSESSOR_USER_EMAIL)->delete();

        /** @var ZipSubmission $job */
        $job = ZipSubmission::factory()->create();
        $this->artisan(IssueJobTokens::COMMAND . ' ' . $job->id)
            ->expectsOutput('Assessor user does not exist: please run the AssessorUserSeeder')
            ->assertExitCode(2);
    }

    public function testIssueOK() {
        Storage::fake();
        $this->seed();

        /** @var ZipSubmission $job */
        $job = ZipSubmission::factory()->create();
        $this->artisan(IssueJobTokens::COMMAND . ' ' . $job->id)
            ->assertExitCode(0);
    }
}
