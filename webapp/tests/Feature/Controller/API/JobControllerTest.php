<?php

/**
 *  Copyright 2021 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace Tests\Feature\Controller\API;


use App\AssessmentSubmission;
use App\BuildResultFile;
use App\Http\Controllers\API\JobController;
use App\Jobs\MarkSubmissionJob;
use App\Policies\AssessmentPolicy;
use App\Policies\ZipSubmissionPolicy;
use App\TeachingModuleUser;
use App\Zip\ExtendedZipArchive;
use App\ZipSubmission;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\TestResponse;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class JobControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var AssessmentSubmission
     */
    private $asub;

    /**
     * @var TeachingModuleUser
     */
    private $tmu;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        Storage::fake();
        Queue::fake();

        $this->asub = AssessmentSubmission::factory()->create();
        $this->tmu = TeachingModuleUser::factory()->create([
            'teaching_module_id' => $this->asub->assessment->usage->teaching_module_id,
        ]);
    }

    public function testDownloadRequiresAuth() {
        $response = $this->doDownloadBuildZip();
        $response->assertUnauthorized();
    }

    public function testDownloadRequiresCapability() {
        Sanctum::actingAs($this->asub->author->user, ['something:else']);
        $response = $this->doDownloadBuildZip();
        $response->assertForbidden();
    }

    public function testDownloadRequiresPermissionsNone()
    {
        $this->setUpBuildZipSanctumToken();
        $response = $this->doDownloadBuildZip();
        $response->assertForbidden();
    }

    public function testDownloadRequiresPermissionsOnlyViewModelSolution()
    {
        $this->setUpBuildZipSanctumToken();

        $this->tmu->syncPermissions([AssessmentPolicy::VIEW_MODEL_SOLUTION_PERMISSION]);
        $response = $this->doDownloadBuildZip();
        $response->assertForbidden();
    }

    public function testDownloadRequiresPermissionsOnlyViewSubmission()
    {
        $this->setUpBuildZipSanctumToken();
        $this->tmu->syncPermissions([ZipSubmissionPolicy::VIEW_ANY_PERMISSION]);
        $response = $this->doDownloadBuildZip();
        $response->assertForbidden();
    }

    public function testDownloadRequiresPermissionsAll()
    {
        $this->setUpBuildZipSanctumToken();

        $this->tmu->syncPermissions([
            ZipSubmissionPolicy::VIEW_ANY_PERMISSION,
            AssessmentPolicy::VIEW_MODEL_SOLUTION_PERMISSION
        ]);

        $response = $this->doDownloadBuildZip();
        $response->assertSuccessful();
        $response->assertDownload('build-sub_' . $this->asub->submission->id . '.zip');
        $response->assertHeader('Content-Type', 'application/zip');
    }

    public function testDownloadRequiresValidIP() {
        $this->setUpBuildZipSanctumToken();

        $this->tmu->syncPermissions([
            ZipSubmissionPolicy::VIEW_ANY_PERMISSION,
            AssessmentPolicy::VIEW_MODEL_SOLUTION_PERMISSION
        ]);

        $this->withServerVariables(['REMOTE_ADDR' => '1.1.1.1']);
        $response = $this->doDownloadBuildZip();
        $response->assertForbidden();
    }

    public function testStoreResultsRequiresAuth() {
        $response = $this->doStoreResults();
        $response->assertUnauthorized();
    }

    public function testStoreResultsRequiresCapability() {
        Sanctum::actingAs($this->asub->author->user, ['something:else']);
        $response = $this->doStoreResults();
        $response->assertForbidden();
    }

    public function testStoreResultsRequiresPermissionNone() {
        $this->setUpStoreResultsSanctumToken();
        $response = $this->doStoreResults();
        $response->assertForbidden();
    }

    public function testStoreResultsRequiresPermissionOnlyView() {
        $this->setUpStoreResultsSanctumToken();
        $this->tmu->syncPermissions([ZipSubmissionPolicy::VIEW_ANY_PERMISSION]);
        $response = $this->doStoreResults();
        $response->assertForbidden();
    }

    public function testStoreResultsRequiresPermissionOnlyUploadResults() {
        $this->setUpStoreResultsSanctumToken();
        $this->tmu->syncPermissions([ZipSubmissionPolicy::STORE_RESULTS_PERMISSION]);
        $response = $this->doStoreResults();
        $response->assertForbidden();
    }

    public function testStoreResultsRequiresPermissionAll() {
        $this->setUpStoreResultsSanctumToken();
        $this->tmu->syncPermissions([
            ZipSubmissionPolicy::VIEW_ANY_PERMISSION,
            ZipSubmissionPolicy::STORE_RESULTS_PERMISSION
        ]);

        // The API requires a file
        $response = $this->doStoreResults();
        $response->assertStatus(400);
        $uploadedFile = $this->createResultsZIP();

        // Request also validates the provided status code
        $response = $this->doStoreResults($uploadedFile, 'invalid');
        $response->assertStatus(400);
        $this->assertEquals(0, $this->asub->submission->resultFiles()->count());
        Queue::assertNothingPushed();

        // Now, do the actual submission
        $statusCode = 123;
        $response = $this->doStoreResults($uploadedFile, $statusCode);
        $response->assertSuccessful();
        $this->asub->submission->refresh();
        $this->assertEquals($statusCode, $this->asub->submission->status);

        // DB ENTRIES

        $this->assertEquals(4, $this->asub->submission->resultFiles()->count(),
            "4 result files should have been collected: stderr, stdout, one JUnit XML, and the Jacoco file");
        $this->assertEquals(1, $this->asub->submission->resultFiles()
            ->where('originalPath', '/stderr.txt')->where('source', 'stderr')->count());
        $this->assertEquals(1, $this->asub->submission->resultFiles()
            ->where('originalPath', '/stdout.txt')->where('source', BuildResultFile::SOURCE_STDOUT)->count());
        $this->assertEquals(1, $this->asub->submission->resultFiles()
            ->where('originalPath', '/a.xml')->where('source', BuildResultFile::SOURCE_JUNIT)->count());
        $this->assertEquals(1, $this->asub->submission->resultFiles()
            ->where('originalPath', '/b.txt')->where('source', 'jacoco')->count());

        // QUEUED JOBS

        Queue::assertPushed(MarkSubmissionJob::class, 1);
        Queue::assertPushedOn(ZipSubmission::QUEUE_NON_JAVA, MarkSubmissionJob::class,
            function(MarkSubmissionJob $job) {
                return $job->aSubmissionId == $this->asub->id;
            });
    }

    public function testStoreResultsRequiresValidIP() {
        $this->setUpStoreResultsSanctumToken();
        $this->tmu->syncPermissions([
            ZipSubmissionPolicy::VIEW_ANY_PERMISSION,
            ZipSubmissionPolicy::STORE_RESULTS_PERMISSION
        ]);

        // The API needs to be invoked from a whitelisted IP range
        $this->withServerVariables(['REMOTE_ADDR' => '1.1.1.1']);

        $uploadedFile = $this->createResultsZIP();
        $response = $this->doStoreResults($uploadedFile, 0);
        $response->assertForbidden();
        $this->assertEquals(0, $this->asub->submission->resultFiles()->count());
        Queue::assertNothingPushed();
    }

    private function doDownloadBuildZip(): TestResponse
    {
        return $this->json('GET', route('api.jobs.downloadBuildZip', $this->asub->submission->id));
    }

    private function setUpBuildZipSanctumToken(): void
    {
        Sanctum::actingAs($this->tmu->user,
            [JobController::DOWNLOAD_COMBINED_CAP_PREFIX . $this->asub->submission->id]);
    }

    private function doStoreResults($uploadedFile = null, $statuscode = 0): TestResponse
    {
        return $this->json('POST', route('api.jobs.storeResults', $this->asub->submission->id), [
            'resultsfile' => $uploadedFile,
            'statuscode' => $statuscode,
        ]);
    }

    private function setUpStoreResultsSanctumToken(): void
    {
        Sanctum::actingAs($this->tmu->user,
            [JobController::STORE_RESULTS_CAP_PREFIX . $this->asub->submission->id]);
    }

    /**
     * @return \Illuminate\Http\Testing\File
     */
    private function createResultsZIP(): \Illuminate\Http\Testing\File
    {
        // Create a results ZIP file
        $tempResultsFolder = tempnam(sys_get_temp_dir(), 'results');
        unlink($tempResultsFolder);
        mkdir($tempResultsFolder);
        file_put_contents("$tempResultsFolder/stderr.txt", "standard error");
        file_put_contents("$tempResultsFolder/stdout.txt", "standard output");
        mkdir("$tempResultsFolder/junit");
        file_put_contents("$tempResultsFolder/junit/a.xml", "<suite/>");
        file_put_contents("$tempResultsFolder/junit/a.txt", "something else");
        mkdir("$tempResultsFolder/jacoco");
        file_put_contents("$tempResultsFolder/jacoco/b.txt", "another thing");
        $zipPath = "test_store_results.zip";
        ExtendedZipArchive::zipTree($tempResultsFolder, $zipPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $uploadedFile = UploadedFile::fake()->createWithContent('results.zip', file_get_contents($zipPath));
        return $uploadedFile;
    }

}
