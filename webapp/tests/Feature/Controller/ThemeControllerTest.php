<?php

/**
 *  Copyright 2020-2022 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Tests\Feature\Controller;

use App\Http\Controllers\ThemeController;
use Tests\TestCase;

class ThemeControllerTest extends TestCase
{
    public function testToggle()
    {
        $this->assertTrue(ThemeController::isLightTheme());
        $response = $this->post(route('theme.toggle'));
        $response->assertRedirect();

        $this->assertFalse(ThemeController::isLightTheme());
        $response = $this->post(route('theme.toggle'));
        $response->assertRedirect();

        $this->assertTrue(ThemeController::isLightTheme());
    }
}
