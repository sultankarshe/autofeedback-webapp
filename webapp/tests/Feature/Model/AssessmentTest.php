<?php

/**
 *  Copyright 2020-2023 Aston University, University of York
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Tests\Feature\Model;

use App\Assessment;
use App\AssessmentSubmission;
use App\FileOverride;
use App\Jobs\MarkSubmissionJob;
use App\ZipSubmission;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class AssessmentTest extends TestCase
{
    use RefreshDatabase;

    public function testModelSubmissionRelation()
    {
        /** @var Assessment $assessment */
        $assessment = Assessment::factory()->create();
        $this->assertEquals($assessment->id,
            $assessment->latestModelSolution->submission->modelSolution->assessment->id);
    }

    public function testCannotDeleteWithModelSolutions() {
        /** @var Assessment $assessment */
        $assessment = Assessment::factory()->create();
        $this->assertNotNull($assessment->latestModelSolution);
        $this->assertFalse($assessment->delete());
    }

    public function testCanDeleteWithoutModelSolutions() {
        $assessment = $this->createWithoutModelSolution();
        $this->assertCanDelete($assessment);
    }

    public function testCanDeleteWithoutModelSolutionsWithOverrides() {
        $assessment = $this->createWithoutModelSolution();
        FileOverride::create([
            'assessment_id' => $assessment->id,
            'path' => '/foo'
        ]);
        $this->assertCanDelete($assessment);
    }

    public function testCanDeleteWithoutModelSolutionsWithTests() {
        $assessment = $this->createWithoutModelSolution();
        \App\AssessmentTest::factory()->create([
            'assessment_id' => $assessment->id,
        ]);
        $this->assertCanDelete($assessment);
    }

    /**
     * @param Assessment $assessment
     */
    private function assertCanDelete(Assessment $assessment): void
    {
        $this->assertTrue($assessment->delete());
        $this->assertDeleted($assessment);
    }

    /**
     * @return Assessment
     */
    private function createWithoutModelSolution(): Assessment
    {
        /** @var Assessment $assessment */
        $assessment = Assessment::factory()->create();
        $assessment->latestModelSolution->delete();
        return $assessment;
    }

    public function testExportImportNoMarksChange() {
        Queue::fake();

        /** @var Assessment $assessment */
        $assessment = Assessment::factory()->create();
        /** @var \App\AssessmentTest[] $tests */
        $tests = \App\AssessmentTest::factory()->count(3)->create([
            'assessment_id' => $assessment->id
        ]);
        AssessmentSubmission::factory()->create([
            'assessment_id' => $assessment->id
        ]);

        // Customize the various tests
        $newFeedback = ["This is a test", "This is a second test", "This is a third test"];
        foreach ($newFeedback as $i => $text) {
            $tests[$i]->feedback_markdown = $text;
            $tests[$i]->save();
        }

        // Export them
        $exportData = $assessment->exportTestData();

        // Reset the tests
        foreach ($newFeedback as $i => $text) {
            $tests[$i]->feedback_markdown = '';
            $tests[$i]->save();
            $assessment->refresh();
        }

        // Import the data back
        $messages = $assessment->importTestData($exportData);
        $this->assertEmpty($messages);
        Queue::assertNothingPushed();

        // We should have the custom feedback templates back
        foreach ($newFeedback as $i => $text) {
            $tests[$i]->refresh();
            $this->assertEquals($text, $tests[$i]->feedback_markdown);
        }
    }

    public function testExportImportMarksChange() {
        Queue::fake();

        /** @var Assessment $assessment */
        $assessment = Assessment::factory()->create();
        /** @var \App\AssessmentTest[] $tests */
        $tests = \App\AssessmentTest::factory()->count(3)->create([
            'assessment_id' => $assessment->id
        ]);
        /** @var AssessmentSubmission $asub */
        $asub = AssessmentSubmission::factory()->create([
            'assessment_id' => $assessment->id
        ]);

        // Customize the mark for the second test
        $oldPoints = $tests[1]->points;
        $newPoints = bcadd($tests[1]->points, '1.00', 2);
        $tests[1]->points = $newPoints;
        $tests[1]->save();

        // Export and change the mark back
        $exportData = $assessment->exportTestData();
        $tests[1]->points = $oldPoints;
        $tests[1]->save();
        $assessment->refresh();

        // Now import, check the changes, and check that the re-marking job was scheduled
        $messages = $assessment->importTestData($exportData);
        $this->assertEquals(1, sizeof($messages));
        $this->assertTrue(str_contains($messages[0], 're-marking'));
        Queue::assertPushedOn(ZipSubmission::QUEUE_NON_JAVA,  MarkSubmissionJob::class,
            function (MarkSubmissionJob $job) use ($asub) {
                return $job->aSubmissionId === $asub->id;
            });

        $tests[1]->refresh();
        $this->assertEquals($newPoints, $tests[1]->points);
    }

    public function testImportNoClassName() {
        Queue::fake();

        /** @var Assessment $assessment */
        $assessment = Assessment::factory()->create();

        $messages = $assessment->importTestData([[]]);
        $this->assertEquals(1, sizeof($messages));
        $this->assertTrue(str_contains($messages[0], 'class name'));
    }

    public function testImportNoTestName() {
        Queue::fake();

        /** @var Assessment $assessment */
        $assessment = Assessment::factory()->create();

        $messages = $assessment->importTestData([[
            'class_name' => 'something'
        ]]);
        $this->assertEquals(1, sizeof($messages));
        $this->assertTrue(str_contains($messages[0], 'test name'));
    }

    public function testImportDuplicatedEntry() {
        Queue::fake();

        /** @var Assessment $assessment */
        $assessment = Assessment::factory()->create();

        $messages = $assessment->importTestData([[
            'class_name' => 'something',
            'name' => 'method1'
        ], [
            'class_name' => 'something',
            'name' => 'method1'
        ]]);
        $this->assertEquals(1, sizeof($messages));
        $this->assertTrue(str_contains($messages[0], 'duplicated entry'));
    }
}
