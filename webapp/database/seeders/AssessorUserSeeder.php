<?php

/**
 *  Copyright 2021 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Database\Seeders;

use App\Policies\AssessmentPolicy;
use App\Policies\ZipSubmissionPolicy;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * Creates the special "assessor" user, which will be issued tokens to be used by Kubernetes
 * to download build ZIPs and upload build results.
 */
class AssessorUserSeeder extends Seeder
{
    const ASSESSOR_USER_EMAIL = 'assessor@autofeedback.local';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */
        $user = User::firstOrCreate([
            'email' => self::ASSESSOR_USER_EMAIL,
        ], [
            'name' => 'System Assessor',
            'password' => Hash::make(Str::random(64)),
            'disabled_login' => true,
        ]);

        /* This assessor cannot login via the website, but can use single-use tokens. */
        $user->syncPermissions([
            AssessmentPolicy::VIEW_MODEL_SOLUTION_PERMISSION,
            ZipSubmissionPolicy::VIEW_ANY_PERMISSION,
            ZipSubmissionPolicy::STORE_RESULTS_PERMISSION,
        ]);
    }

}
