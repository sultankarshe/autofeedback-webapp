@php
    /**
     *  Copyright 2023 University of York
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     * http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */
@endphp

@extends('layouts.app')

@section('title', __('Import YAML - :title', ['title' => $assessment->usage->title]))

@php
    /**
     * @var \App\TeachingModule $module
     * @var \App\Assessment $assessment
     */
@endphp

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <x-item-path :module="$module" :item="$assessment->usage">
                <li class="breadcrumb-item"><a
                        href="{{ route('modules.assessments.showTests', ['module' => $module->id, 'assessment' => $assessment->id ]) }}">{{ __('Tests') }}</a>
                </li>
                <li class="breadcrumb-item active">{{ __('Import') }}</li>
            </x-item-path>
        </div>

        <div class="col-md-6">
            @if($assessment->tests()->count() > 0)
                <form method="POST"
                      action="{{ route('modules.assessments.importTestsFromYAML', ['module' => $module->id, 'assessment' => $assessment->id]) }}"
                      id="importForm" enctype="multipart/form-data">
                    @csrf

                    <div class="text-center mb-4">
                        <p>{{ __('Upload a YAML file in UTF-8 encoding and LF-style newlines below. :appname will use it to update the configuration of your existing tests.', ['appname' => config('app.name')]) }}</p>

                    <x-file-upload-field :fieldName="'yamlfile'" :required="true" :promptText="'Choose YAML file'"
                                             :accept="'application/yaml,.yaml,.yml'"/>
                    </div>

                    <div class="text-center mt-4">
                        <input type="submit" class="btn btn-primary" value="{{ __('Upload') }}"/>
                        <a role="button" class="btn btn-secondary" href="{{ route('modules.assessments.showTests', ['module' => $module->id, 'assessment' => $assessment->id ]) }}">{{ __('Cancel') }}</a>
                    </div>
                </form>
            @else
                <div class="alert alert-warning mt-2" role="alert">
                    {{ __('No model solution has been set up yet, or the model solution has no tests.') }}
                </div>
            @endif
        </div>
    </div>
@endsection
