#!/bin/bash

SCRIPT_DIR="$(dirname $(readlink -f "$0"))"
cd "$SCRIPT_DIR"

# Download the latest images for the tag selected in the Docker Compose files
./prod-compose.sh pull

# Stop nginx temporarily (helps with IPv4-related issues in some deployments)
./prod-compose.sh stop nginx

# Recreate any running containers using new images
./prod-compose.sh up -d
