/**
 *  Copyright 2022 University of York
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.autofeedback.sample;

import java.util.Scanner;

/**
 * Sample alternative solution to the Roman number parser, without using regular expressions or LinkedHashMaps.
 * You can try removing parts or adding bugs to see how the AutoFeedback report changes.
 */
public class RomanNumberParser {

	/** Smallest integer representable as a Roman number. */
	public static final int MIN_VALUE = 1;

	/** Largest integer representable as a Roman number. */
	public static final int MAX_VALUE = 3999;

	private static final String[][] PREFIXES = {
			{ "M" },
			{"CM", "CD", "D", "C"},
			{"XC", "XL", "L", "X"},
			{"IX", "IV", "V", "I"}
	};
	private static final int[][] VALUES = {
			{ 1000 },
			{ 900, 400, 500, 100 },
			{ 90, 40, 50, 10 },
			{ 9, 4, 5, 1 }
	};

	public int fromRoman(String roman) {
		int value = 0;
		for (int m = 0; m < 3; ++m) {
			if (roman.startsWith(PREFIXES[0][0])) {
				value += VALUES[0][0];
				roman = roman.substring(PREFIXES[0][0].length());
			}
		}

		outer:
		for (int i = 1; i < VALUES.length; i++) {
			for (int j = 0; j < 2; ++j) {
				if (roman.startsWith(PREFIXES[i][j])) {
					value += VALUES[i][j];
					roman = roman.substring(PREFIXES[i][j].length());
					continue outer;
				}
			}

			if (roman.startsWith(PREFIXES[i][2])) {
				value += VALUES[i][2];
				roman = roman.substring(PREFIXES[i][2].length());
			}
			for (int k = 0; k < 3; ++k) {
				if (roman.startsWith(PREFIXES[i][3])) {
					value += VALUES[i][3];
					roman = roman.substring(PREFIXES[i][3].length());
				}
			}
		}

		if (!roman.isBlank() || value == 0) {
			throw new IllegalArgumentException(roman + " is not a valid Roman number");
		}

		return value;
	}

	public String toRoman(int value) {
		if (value < MIN_VALUE || value > MAX_VALUE) throw new IllegalArgumentException("Cannot convert " + value);

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < VALUES.length; i++) {
			// This loop handles how we put IV before V
			for (int k = 0; k < VALUES[i].length; ++k) {
				String maxPrefix = null;
				int maxPrefixValue = 0;
				for (int j = 0; j < VALUES[i].length; j++) {
					if (value >= VALUES[i][j] && VALUES[i][j] > maxPrefixValue) {
						maxPrefix = PREFIXES[i][j];
						maxPrefixValue = VALUES[i][j];
					}
				}
				if (maxPrefixValue > 0) {
					while (value >= maxPrefixValue) {
						sb.append(maxPrefix);
						value -= maxPrefixValue;
					}
				} else {
					break;
				}
			}
		}

		return sb.toString();
	}

	public static void main(String[] args) {
		final RomanNumberParser parser = new RomanNumberParser();
		try (Scanner sc = new Scanner(System.in)) {
			boolean done = false;
			while (!done) {
				done = true;
				System.out.println("Type either 'from ROMAN' or 'to ARABIC' and Enter to try out your converter, or enter anything else to quit: ");

				if (sc.hasNextLine()) {
					String[] parts = sc.nextLine().split("\\s+");
					if (parts.length == 2) {
						switch (parts[0]) {
						case "from":
							try {
								int result = parser.fromRoman(parts[1]);
								System.out.println(String.format("fromRoman(%s) produced %d", parts[1], result));
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							done = false;
							break;
						case "to":
							try {
								String result = parser.toRoman(Integer.parseInt(parts[1]));
								System.out.println(String.format("toRoman(%s) produced '%s'", parts[1], result));
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							done = false;
							break;
						}
					}
				}
			}
		}
	}

}
