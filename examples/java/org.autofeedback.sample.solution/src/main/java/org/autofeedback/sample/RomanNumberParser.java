/**
 *  Copyright 2022 University of York
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.autofeedback.sample;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RomanNumberParser {

	/** Smallest integer representable as a Roman number. */
	public static final int MIN_VALUE = 1;

	/** Largest integer representable as a Roman number. */
	public static final int MAX_VALUE = 3999;

	private static final Pattern REGEX_ROMAN = Pattern.compile(
		"(?<thousands>M{0,3})?(?<hundreds>CM|C?D|D?C{1,3})?(?<tens>XC|X?L|L?X{1,3})?(?<units>IX|I?V|V?I{1,3})?");

	private static final Map<String, Integer> VALUES = new LinkedHashMap<>();
	static {
		VALUES.put("M", 1000);
		VALUES.put("CM", 900);
		VALUES.put("D", 500);
		VALUES.put("CD", 400);
		VALUES.put("C", 100);
		VALUES.put("XC", 90);
		VALUES.put("L", 50);
		VALUES.put("XL", 40);
		VALUES.put("X", 10);
		VALUES.put("IX", 9);
		VALUES.put("V", 5);
		VALUES.put("IV", 4);
		VALUES.put("I", 1);
	}

	public int fromRoman(String roman) {
		if (roman.isBlank()) {
			throw new IllegalArgumentException("An empty or blank string is not a valid Roman number");
		}

		Matcher m = REGEX_ROMAN.matcher(roman);
		if (!m.matches()) {
			throw new IllegalArgumentException(String.format("%s is not a valid Roman number", roman));
		}

		int value = 0;
		int pos = 0;
		outer:
		while (pos < roman.length()) {
			String sub = roman.substring(pos);
			for (Entry<String, Integer> e : VALUES.entrySet()) {
				if (sub.startsWith(e.getKey())) {
					value += e.getValue();
					pos += e.getKey().length();
					continue outer;
				}
			}
			throw new IllegalArgumentException(String.format("Unrecognized substring '%s' in '%s'", sub, roman));
		}

		return value;
	}

	public String toRoman(int number) {
		if (number < MIN_VALUE) {
			throw new IllegalArgumentException(String.format("%d is too small for a Roman number: minimum representable value is 1 (I)", number)); 
		} else if (number > MAX_VALUE) {
			throw new IllegalArgumentException(String.format("%d is too large for a Roman number: maximum representable value is 3999 (MMMCMXCIX)", number));
		}

		StringBuffer sb = new StringBuffer();
		for (Entry<String, Integer> entry : VALUES.entrySet()) {
			while (number >= entry.getValue()) {
				sb.append(entry.getKey());
				number -= entry.getValue();
			}
		}

		assert number == 0 : "The number should have been fully exhausted at the end of the function";
		return sb.toString();
	}

	public static void main(String[] args) {
		final RomanNumberParser parser = new RomanNumberParser();
		try (Scanner sc = new Scanner(System.in)) {
			boolean done = false;
			while (!done) {
				done = true;
				System.out.println("Type either 'from ROMAN' or 'to ARABIC' and Enter to try out your converter, or enter anything else to quit: ");

				if (sc.hasNextLine()) {
					String[] parts = sc.nextLine().split("\\s+");
					if (parts.length == 2) {
						switch (parts[0]) {
						case "from":
							try {
								int result = parser.fromRoman(parts[1]);
								System.out.println(String.format("fromRoman(%s) produced %d", parts[1], result));
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							done = false;
							break;
						case "to":
							try {
								String result = parser.toRoman(Integer.parseInt(parts[1]));
								System.out.println(String.format("toRoman(%s) produced '%s'", parts[1], result));
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							done = false;
							break;
						}
					}
				}
			}
		}
	}
}
